package dev.connect.web.indicator;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeIndicatorResourceIT extends IndicatorResourceTest {

    // Execute the same tests but in native mode.
}